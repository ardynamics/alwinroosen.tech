const puppeteer = require('puppeteer');
const fs = require('fs');

async function printPDF() {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.addStyleTag({ content: '@page { size: 900; }' });
  await page.goto('http://localhost:4200', {
    timeout: 25000,
    waitUntil: ['load', 'domcontentloaded', 'networkidle2'],
  });
  await page.emulateMediaType('print');
  const pdf = await page.pdf({ scale: 1, format: 'a4', printBackground: true, pageRanges: '', preferCSSPageSize: true });

  await browser.close();
  return pdf;
}

printPDF().then((pdf) => {
  fs.writeFileSync('dist/2020_CV_Alwin_Roosen.pdf', pdf, 'binary');
});
