import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from '@environment';

if (environment.production) {
  enableProdMode();
}

declare interface Window {
  app: {
    debug: false;
  };
}

platformBrowserDynamic().bootstrapModule(AppModule);
