import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
    selector: 'app-showcase',
    templateUrl: './showcase.component.html',
    styleUrls: ['./showcase.component.scss'],
    standalone: false
})
export class ShowcaseComponent implements OnInit {
  constructor(private bottomSheetRef: MatBottomSheetRef<ShowcaseComponent>) {}

  ngOnInit() {}

  onClose() {
    this.bottomSheetRef.dismiss();
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
