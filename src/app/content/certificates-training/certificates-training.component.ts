import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-certificates-training',
    templateUrl: './certificates-training.component.html',
    styleUrls: ['./certificates-training.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CertificatesTrainingComponent {}
