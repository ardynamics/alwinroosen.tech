import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddictedComponent } from './addicted.component';

describe('AddictedComponent', () => {
  let component: AddictedComponent;
  let fixture: ComponentFixture<AddictedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddictedComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddictedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
