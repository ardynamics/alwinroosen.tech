import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-addicted',
    templateUrl: './addicted.component.html',
    styleUrls: ['./addicted.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false
})
export class AddictedComponent {}
