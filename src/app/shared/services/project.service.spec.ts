import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ProjectService } from './project.service';
import { LoggerService } from './logger.service';
import { Project } from '../models';

describe('ProjectService', () => {
  let injector: TestBed;
  let service: ProjectService;
  let httpMock: HttpTestingController;
  let logger: LoggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoggerService, ProjectService],
    });
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    logger = injector.get(LoggerService);
    service = injector.get(ProjectService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getProjects', () => {
    it('should return an Observable<Project[]>', () => {
      const dummyProjects = [new Project('domain1', 'name1', null, null, null), new Project('domain2', 'name2', null, null, null, true)];

      service.getProjects().subscribe((projects) => {
        expect(projects.length).toBe(2);
        expect(projects).toEqual(dummyProjects);
        expect(projects[0].isFeatured()).toEqual(false);
        expect(projects[1].isFeatured()).toEqual(true);
      });

      const req = httpMock.expectOne(`${service.endpoint}`);
      expect(req.request.method).toBe('GET');
      req.flush(dummyProjects);
    });
  });
});
