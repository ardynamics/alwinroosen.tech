export class OnScroll {
  static readonly type = '[Layout] OnScroll';
  constructor(public scrollTop: number) {}
}
