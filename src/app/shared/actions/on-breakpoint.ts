import { BreakpointState } from '@angular/cdk/layout';

export class OnBreakpoint {
  static readonly type = '[Layout] OnBreakpoint';
  public constructor(public state: BreakpointState) {}
}
