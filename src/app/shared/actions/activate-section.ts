import { Section } from '@models';

export class ActivateSection {
  static readonly type = '[Layout] ActivateSection';
  constructor(public section: Section) {}
}
