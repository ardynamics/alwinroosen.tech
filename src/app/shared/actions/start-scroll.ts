export class StartScroll {
  static readonly type = '[Layout] StartScroll';
  constructor(public scrollTop: number) {}
}
