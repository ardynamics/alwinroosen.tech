export class StopScroll {
  static readonly type = '[Layout] StopScroll';
  constructor(public scrollTop: number) {}
}
