export interface Skill {
  name: string;
}

export interface SkillGroup {
  id: string;
  name: string;
  skills: Skill[];
}
