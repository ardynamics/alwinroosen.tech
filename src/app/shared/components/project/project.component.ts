import { Component, computed, ElementRef, HostListener, input, Renderer2, signal } from '@angular/core';
import { Project } from '@models';
import { ProjectService } from '@services';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  standalone: false,
})
export class ProjectComponent {
  project = input.required<Project>();

  isFeatured = input(false);

  showDetails = signal(false);
  normalImage = computed(() => this.project().getImageUri());
  webpImage = computed(() => this.project().getImageUri(true));

  constructor(
    private readonly projectService: ProjectService,
    private readonly renderer: Renderer2,
    private readonly container: ElementRef,
  ) {}

  @HostListener('mouseenter')
  onMouseEnter() {
    this.renderer.addClass(this.container.nativeElement, 'zoom');
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.renderer.removeClass(this.container.nativeElement, 'zoom');
  }

  onClick() {
    if (this.isFeatured()) {
      window.open(this.project().link);
    } else {
      this.showDetails.update((value) => {
        this.projectService.onProjectSelected.next({});
        if (!value) {
          return true;
        }
      });
    }
  }
}
