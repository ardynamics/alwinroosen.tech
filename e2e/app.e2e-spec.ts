import { AlwinroosenTechPage } from './app.po';

describe('alwinroosen.tech App', () => {
  let page: AlwinroosenTechPage;

  beforeEach(() => {
    page = new AlwinroosenTechPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
