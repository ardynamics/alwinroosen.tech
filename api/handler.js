const dynamodb = require('serverless-dynamodb-client');
const quotesData = require('./data/quotes');
const skillsData = require('./data/skills');

module.exports.getProjects = (event, context, callback) => {
  const payload = [];

  const params = {
    TableName: process.env.TABLE_NAME
  };

  if( typeof(event.featured) !== "undefined" && event.featured ) {

    params.ExpressionAttributeValues = {
      ':is_featured': {
        BOOL: true
      }
    };

    params.FilterExpression = 'featured = :is_featured';

  }

  dynamodb.raw.scan(params, function (err, data) {

    if (err) {
      const errMsg = 'Unable to query. Error: ' + JSON.stringify(err, null, 2);
      console.error(errMsg);
      return callback(errMsg);
    }

    data.Items.forEach(function (item) {

      var project = {
        id: item.id.S,
        name: item.name.S,
        domain: typeof(item.domain) !== 'undefined' ? item.domain.S : null,
        description: typeof(item.description) !== 'undefined' ? item.description.S : null,
        image: typeof(item.image) !== 'undefined' ? item.image.S : null,
        link: typeof(item.link) !== 'undefined' ? item.link.S : null,
        featured: typeof(item.featured) !== 'undefined' ? item.featured.BOOL : false,
        highlights: []
      };

      if(typeof(item.highlights) !== 'undefined') {

        for(var i=0; i<item.highlights.L.length; i++) {

          project.highlights.push(item.highlights.L[i].S);

        }

      }

      payload.push(project);

    });

    if( typeof(event.featured) !== "undefined" && event.featured ) {

      payload.sort((a, b) => {
        return parseInt(b.id, 10) - parseInt(a.id, 10);
      });

    }

    const response = {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      body: JSON.stringify(payload),
    };

    callback(null, response);
  });
};

module.exports.getQuotes = (event, context, callback) => {
  const response = {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(quotesData),
  };
  callback(null, response);
};

module.exports.getSkills = (event, context, callback) => {
  const response = {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(skillsData),
  };
  callback(null, response);
};
