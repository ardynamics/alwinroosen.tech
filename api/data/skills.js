module.exports = [
  {
    "id": "frontend",
    "name": "Front-End",
    "skills": [
      {
        "name": "JavaScript / ES6"
      },
      {
        "name": "TypeScript"
      },
      {
        "name": "Angular 2-10"
      },
      {
        "name": "React (-)"
      },
      {
        "name": "CSS3 / HTML5"
      },
      {
        "name": "jQuery"
      },
      {
        "name": "Bootstrap / Foundation"
      },
      {
        "name": "Material Design"
      }
    ]
  },
  {
    "id": "backend",
    "name": "Back-End",
    "skills": [
      {
        "name": "MySQL / MariaDB"
      },
      {
        "name": "DynamoDB / MongoDB"
      },
      {
        "name": "PHP 5+"
      },
      {
        "name": "Symfony / Laravel"
      },
      {
        "name": "REST / XML"
      },
      {
        "name": "Node.js"
      },
      {
        "name": "C# .NET (-)"
      },
      {
        "name": "Python (-)"
      }
    ]
  },
  {
    "id": "environment",
    "name": "Environment",
    "skills": [
      {
        "name": "npm / yarn / bower"
      },
      {
        "name": "Less / Sass"
      },
      {
        "name": "Gulp / Grunt"
      },
      {
        "name": "Bazel / Cypress / Jest"
      },
      {
        "name": "Vagrant / PuPHPet"
      },
      {
        "name": "Docker / Microservices"
      },
      {
        "name": "Git / BitBucket / CI-CD"
      },
      {
        "name": "Wordpress / WooCommerce"
      },
      {
        "name": "JIRA / Confluence"
      },
      {
        "name": "Lean / Agile"
      }
    ]
  },
  {
    "id": "infrastructure",
    "name": "Infrastructure",
    "skills": [
      {
        "name": "FreeBSD / Linux"
      },
      {
        "name": "LAMP / Nginx / Varnish"
      },
      {
        "name": "Serverless Apps"
      },
      {
        "name": "Saas / Cloud Computing"
      },
      {
        "name": "sh / bash / perl / python"
      },
      {
        "name": "Gitlab / DevOps / BitBucket"
      },
      {
        "name": "Amazon Web Services"
      },
      {
        "name": "GCloud / Kubernetes (-)"
      },
      {
        "name": "Azure (-)"
      }
    ]
  }
];
