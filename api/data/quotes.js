module.exports = [
  {
    "author": "Wim Hoste",
    "company": "WeGrafics",
    "quote": "Strong points of Alwin are: professional approach, good advice and rapid completion as promised. Wegrafics has a trustworthy relationship with Alwin for already many successful projects."
  },
  {
    "author": "Sven De Keyser",
    "company": "RDK NV",
    "quote": "We have been struggling to get our website running at optimal conditions. Thanks to the smooth cooperation with Alwin, our new website was ready in no time and exceeded our expectations. Remarks or questions were being handled quickly and correct. Alwin has analysed the project and came up with new ways to improve our website. We are very satisfied with the cooperation and the excellent results."
  },
  {
    "author": "Serge Huybrechts",
    "company": "WeCandor",
    "quote": "During development of my website I was able to give feedback to Alwin in short iterations. It was very exciting to see these changes being implemented rapidly. Great communication, top service!"
  },
  {
    "author": "Els Duflos",
    "company": "Confocus",
    "quote": "It was not easy… finding the perfect partner for our new website. Alwin just needed one meeting to understand our wishes, our concerns and the difficulties we face in developing a working marketing tool for our seminars. This gives me peace of mind to entrust the development of our custom-made website to him.  His solution to connect our database system to our ITC Back-end will save us a lot of time – now and in the future."
  },
  {
    "author": "Nicolaas Bijvoet",
    "company": "Vente-Exclusive",
    "quote": "You're really taking the role that was bestowed upon you. And I can only have great respect for that. You are a blessing to the whole CF team. You bring a certain calm by having a clear view on the task at hand. You are really thorough which is a strenght but can be a pitfall as well in our Vex environment. I really like the way you communicate with the other team members. You have proven to take ownership not only over the product but over the process as well. You are truly one of the (technical) strongholders of the CF team."
  },
  {
    "author": "Lotte Laursen",
    "company": "Vente-Exclusive",
    "quote": "I had the pleasure of working with Alwin at Vente-Exclusive (now Veepee). It was a period where we were faced with an extremely challenging delivery roadmap, both in terms of scope and deadlines. During this time Alwin showed an impressive dedication, and delivered great quality at a very high pace. Alwin keeps the target in mind, stays focused and gets the job done. At the same time Alwin is not afraid of challenging the various stakeholders, proposing alternatives and arrive at a consensus that ensures the needed quality in terms of customer experience. A great contribution to a team!"
  },
  {
    "author": "Fabrice Catry",
    "company": "GS1 Belgium & Luxembourg",
    "quote": "Great guy to have on the project. I had the pleasure to work with him on a complex program at GS1 where he managed the Front End part from conception to delivery, in line with the objectives of the program. He is the right man for the job, if you need somebody to tackle your most challenging assignments in the area of Front End Development."
  },
  {
    "author": "Robin Goossens",
    "company": "GS1 Belgium & Luxembourg",
    "quote": "A man that doesn't back down from a challenge. At GS1 we were very please with the front-end development work of Alwin and his overall contribution in the team. We sometimes presented him with challenging ideas around our product management application and he was always able to either deliver exactly what we wanted or was able to provide alternative solutions that also fulfilled the business needs."
  },
  {
    "author": "Senne Van den Bergh",
    "company": "GS1 Belgium & Luxembourg",
    "quote": "We selected Alwin based on his experience and profound knowledge of the Angular framework. He was able to translate the GS1 vision to a stable but flexible front-end, completely in line with (or sometimes exceeding) our expectations. Alwin is not afraid of complex and challenging objectives and is a team player. He is able to keep a good overview, stays calm under stressful conditions and gets the job done. Alwin is punctual, trustworthy and often comes with great suggestions. Great guy to have on your team!"
  }
];
