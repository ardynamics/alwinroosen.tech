const copy = require('copy-dynamodb-table').copy;

var globalAWSConfig = {
  region: 'eu-central-1'
};

copy({
    config: globalAWSConfig,
    source: {
      tableName: 'alwinroosen.tech.projects',
    },
    destination: {
      tableName: 'alwinroosen-tech-service-prod',
    },
    log: true
  },
  function (err, result) {
    if (err) {
      console.log(err)
    }
    console.log(result)
  });
